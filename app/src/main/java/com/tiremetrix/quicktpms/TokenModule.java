package com.tiremetrix.quicktpms;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by sholliday on 11/5/15.
 */
public class TokenModule {
    private static TokenModule mInstance = null;
    MainActivity act;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public static TokenModule getInstance(){
        if(mInstance == null){
            mInstance = new TokenModule();
        }
        return mInstance;
    }
    public void getToken(Activity fromActivity){
        act = (MainActivity)fromActivity;
        preferences = PreferenceManager.getDefaultSharedPreferences(act);
        editor = preferences.edit();
        String expiration = preferences.getString("exp", "0");
        if(expiration != "0"){
            int exp = Integer.parseInt(expiration);
            if(System.currentTimeMillis()/1000 > exp){
                new GetNewToken().execute();
            }else{
                act.tokenResult(true, "");
            }
        }else{
            new GetNewToken().execute();
        }
    }
    private class GetNewToken extends AsyncTask<Void, Void, Void> {
        ProgressDialog prgDialog = null;
        StringBuilder response = new StringBuilder();
        @Override
        protected void onPreExecute(){
            prgDialog = new ProgressDialog(act);
            prgDialog.setMessage("Retrieving Token...");
            prgDialog.show();
        }
        @Override
        protected Void doInBackground(Void... params){
            try{
                String nonce = randomString(10);
                String secret = "uY-2WJ9K$bG5RZ#hX5YY#$RSVu4$8my$";
                String accessKey = "037CEDD2-2536-4B99-9B38-F7C86A8EF7B6";
                String deviceId = Settings.Secure.getString(act.getContentResolver(), Settings.Secure.ANDROID_ID);
                String deviceInfo = "{\"profile\":\"tm.huf.mobile.android\"}";
                byte[] deviceInfoData = deviceInfo.getBytes("UTF-8");
                deviceInfo = Base64.encodeToString(deviceInfoData, Base64.DEFAULT);
                String toBeHashed = accessKey + "+" + nonce + "+" + deviceId;
                Mac sha_HMAC = Mac.getInstance("HmacSHA512");
                SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA512");
                sha_HMAC.init(secret_key);
                String hash = Base64.encodeToString(sha_HMAC.doFinal(toBeHashed.getBytes()), Base64.DEFAULT);
                try{
                    URL url = new URL("https://id.tiremetrix.com/token");
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    connection.setDoOutput(true);
                    connection.setRequestMethod("POST");
                    String authString = "{\"nonce\":\"" + nonce + "\",\"accessKey\":\"" + accessKey + "\",\"id\":\"" + deviceId + "\",\"info\":\"" + deviceInfo + "\",\"hash\":\"" + hash + "\"}";
                    authString = authString.replace("\n", "");
                    connection.setRequestProperty("Content-Type", "application/json;");
                    connection.setRequestProperty("Content-Length", Integer.toString(authString.length()));
                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                    writer.write(authString);
                    writer.close();
                    String line;
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    while((line = reader.readLine()) != null){
                        response.append(line);
                    }
                    reader.close();
                }catch(IOException e){
                    System.out.println(e.getLocalizedMessage());
                }
            }catch(Exception e){
                System.out.println(e.getLocalizedMessage());
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result){
            if(prgDialog.isShowing()){
                prgDialog.dismiss();
            }
            if(response.length() > 0){
                try{
                    JSONObject json = new JSONObject(response.toString());
                    String jwt = json.getString("accessToken");
                    String[] jwtParts = jwt.split("[.]");
                    byte[] data = Base64.decode(jwtParts[1], Base64.DEFAULT);
                    JSONObject jsonBody = new JSONObject(new String(data, "UTF-8"));
                    editor.putString("aud", jsonBody.getString("aud"));
                    editor.putString("exp", jsonBody.getString("exp"));
                    editor.putString("iat", jsonBody.getString("iat"));
                    editor.putString("iss", jsonBody.getString("iss"));
                    editor.putString("nonce", jsonBody.getString("nonce"));
                    editor.putString("sub", jsonBody.getString("sub"));
                    editor.putString("roles", jsonBody.getString("tm.role"));
                    editor.putString("token", jwt);
                    editor.commit();
                    act.tokenResult(true, "");
                }catch(JSONException e){
                    act.tokenResult(false, e.getLocalizedMessage());
                }catch(UnsupportedEncodingException e){
                    act.tokenResult(false, e.getLocalizedMessage());
                }
            }else{
                act.tokenResult(false, "There was an error retrieving your token. Please try again later.");
            }
        }
    };
    private static String randomString(int length){
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        char tempChar;
        for(int i = 0; i < length; i++){
            tempChar = (char)(generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }
}
