package com.tiremetrix.quicktpms;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by sholliday on 11/5/15.
 */
public class RetrievalModule {
    private static RetrievalModule mInstance = null;
    MainActivity act;
    boolean byVIN;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public static RetrievalModule getInstance(){
        if(mInstance == null){
            mInstance = new RetrievalModule();
        }
        return mInstance;
    }
    public void getTPMS(Activity fromActivity, boolean sentByVIN){
        act = (MainActivity)fromActivity;
        byVIN = sentByVIN;
        preferences = PreferenceManager.getDefaultSharedPreferences(act);
        editor = preferences.edit();
        new GetNewTPMS().execute();
    }
    private class GetNewTPMS extends AsyncTask<Void, Void, Void> {
        ProgressDialog prgDialog = null;
        StringBuilder response = new StringBuilder();
        @Override
        protected void onPreExecute(){
            prgDialog = new ProgressDialog(act);
            prgDialog.setMessage("Retrieving TPMS Info...");
            prgDialog.show();
        }
        @Override
        protected Void doInBackground(Void... params){
            try{
                String token = preferences.getString("token", "0");
                String authString;
                if(byVIN){
                    String vin = preferences.getString("vin", "0");
                    authString = "{\"vin\":\"" + vin + "\",\"token\":\"" + token + "\"}";
                }else{
                    String year = preferences.getString("year", "0");
                    String make = preferences.getString("make", "0");
                    String model = preferences.getString("model", "0");
                    authString = "{\"year\":\"" + year + "\",\"make\":\"" + make + "\",\"model\":\"" + model + "\",\"token\":\"" + token + "\"}";
                }
                try{
                    URL url = new URL("https://id.tiremetrix.com/token");
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    connection.setDoOutput(true);
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("Content-Type", "application/json;");
                    connection.setRequestProperty("Content-Length", Integer.toString(authString.length()));
                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                    writer.write(authString);
                    writer.close();
                    String line;
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    while((line = reader.readLine()) != null){
                        response.append(line);
                    }
                    reader.close();
                }catch(IOException e){
                    System.out.println(e.getLocalizedMessage());
                }
            }catch(Exception e){
                System.out.println(e.getLocalizedMessage());
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result){
            if(prgDialog.isShowing()){
                prgDialog.dismiss();
            }
            if(response.length() > 0){
                try{
                    JSONObject json = new JSONObject(response.toString());
                    //read data
                    act.retrievalResult("Success");
                }catch(JSONException e){
                    act.retrievalResult(e.getLocalizedMessage());
                }
            }else{
                act.retrievalResult("There was an error retrieving your TPMS information. Please try again later.");
            }
        }
    };
}
