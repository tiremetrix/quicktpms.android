package com.tiremetrix.quicktpms;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    InputMethodManager imm;
    public DBHelper dbHelper;
    EditText txtVIN;
    Spinner spnYears;
    Spinner spnMakes;
    Spinner spnModels;
    ArrayList<Year> years;
    ArrayList<Make> makes;
    ArrayList<Model> models;
    Year selectedYear = null;
    Make selectedMake = null;
    Model selectedModel= null;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    boolean byVIN;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent splashIntent = new Intent(MainActivity.this, SplashActivity.class);
        startActivity(splashIntent);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();
        imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
        dbHelper = new DBHelper(this);
        try{
            dbHelper.createDataBase();
        }catch(IOException ioe){
            throw new Error("Unable to create database");
        }
        try{
            dbHelper.openDataBase();
        }catch(SQLException e){
            throw e;
        }
        Typeface typFutura = Typeface.createFromAsset(this.getAssets(),"futura-normal.ttf");
        TextView txtTitle = (TextView)findViewById(R.id.txtTitle);
        txtTitle.setTypeface(typFutura);
        TextView txtSubTitleOne = (TextView)findViewById(R.id.txtSubTitleOne);
        txtSubTitleOne.setTypeface(typFutura);
        txtVIN = (EditText)findViewById(R.id.txtVIN);
        txtVIN.setTypeface(typFutura);
        txtVIN.setSingleLine(true);
        txtVIN.setOnClickListener(clkTxtVIN);
        txtVIN.addTextChangedListener(txtVINWatcher);
        txtVIN.setOnEditorActionListener(txtVINKeyEvents);
        ImageButton btnCamera = (ImageButton)findViewById(R.id.btnCamera);
        btnCamera.setOnClickListener(clkScan);
        TextView txtSubTitleTwo = (TextView)findViewById(R.id.txtSubTitleTwo);
        txtSubTitleTwo.setTypeface(typFutura);
        years = dbHelper.getAllYears();
        ArrayList<String> yearValues = new ArrayList<String>();
        for(Year year : years){
            if(year.ID == 0){
                yearValues.add("Select Year");
            }else{
                yearValues.add(Integer.toString(year.ID));
            }
        }
        spnYears = (Spinner)findViewById(R.id.spnYears);
        ArrayAdapter<String> ada = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, yearValues);
        ada.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnYears.setAdapter(ada);
        spnYears.setOnItemSelectedListener(clkYear);
        spnMakes = (Spinner)findViewById(R.id.spnMakes);
        spnModels = (Spinner)findViewById(R.id.spnModels);
        Button btnRetrieve = (Button)findViewById(R.id.btnRetrieve);
        btnRetrieve.setOnClickListener(clkRetrieve);
        Button btnGoToTPMS = (Button)findViewById(R.id.btnGoToTPMS);
        btnGoToTPMS.setOnClickListener(clkGoToTPMS);
    }
    View.OnClickListener clkTxtVIN = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            byVIN = true;
            clearSpinners();
        }
    };
    TextWatcher txtVINWatcher = new TextWatcher(){
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after){
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count){
        }
        @Override
        public void afterTextChanged(Editable s){
        }
    };
    TextView.OnEditorActionListener txtVINKeyEvents = new TextView.OnEditorActionListener(){
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event){
            if(event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)){
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                TokenModule.getInstance().getToken(MainActivity.this);
                return true;
            }
            return false;
        }
    };
    View.OnClickListener clkScan = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            byVIN = true;
            clearSpinners();
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            IntentIntegrator.initiateScan(MainActivity.this);
        }
    };
    AdapterView.OnItemSelectedListener clkYear = new AdapterView.OnItemSelectedListener(){
        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            imm.hideSoftInputFromWindow(parentView.getWindowToken(), 0);
            if(position > 0){
                byVIN = false;
                txtVIN.setText("");
                selectedYear = years.get(position);
                makes = dbHelper.getMakesByYear(selectedYear.ID);
                ArrayList<String> makeValues = new ArrayList<String>();
                for(Make make : makes){
                    makeValues.add(make.make);
                }
                ArrayAdapter<String> ada = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_spinner_item, makeValues);
                ada.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spnMakes.setAdapter(ada);
                spnMakes.setOnItemSelectedListener(clkMake);
            }else{
                clearSpinners();
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parentView) {
        }
    };
    public void clearSpinners(){
        selectedYear = null;
        selectedMake = null;
        selectedModel = null;
        spnYears.setSelection(0);
        spnMakes.setAdapter(null);
        spnModels.setAdapter(null);
    }
    AdapterView.OnItemSelectedListener clkMake = new AdapterView.OnItemSelectedListener(){
        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            selectedMake = makes.get(position);
            models = dbHelper.getModelsByYearAndMake(selectedYear.ID, selectedMake);
            ArrayList<String> modelValues = new ArrayList<String>();
            for(Model model : models){
                modelValues.add(model.model);
            }
            ArrayAdapter<String> ada = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_spinner_item, modelValues);
            ada.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spnModels.setAdapter(ada);
            spnModels.setOnItemSelectedListener(clkModel);
        }
        @Override
        public void onNothingSelected(AdapterView<?> parentView) {
        }
    };
    AdapterView.OnItemSelectedListener clkModel = new AdapterView.OnItemSelectedListener(){
        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            selectedModel = models.get(position);
        }
        @Override
        public void onNothingSelected(AdapterView<?> parentView) {
        }
    };
    View.OnClickListener clkRetrieve = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            if(byVIN){
                if(txtVIN.getText().length() > 0){
                    editor.putString("vin", txtVIN.getText().toString());
                    editor.commit();
                    TokenModule.getInstance().getToken(MainActivity.this);
                    return;
                }
            }else{
                if(selectedYear != null && selectedMake != null && selectedModel != null){
                    editor.putString("year", Integer.toString(selectedYear.ID));
                    editor.putString("make", selectedMake.make);
                    editor.putString("model", selectedModel.model);
                    editor.commit();
                    TokenModule.getInstance().getToken(MainActivity.this);
                    return;
                }
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setCancelable(true);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){
                    dialog.dismiss();
                }
            });
            builder.setTitle("Error");
            builder.setMessage("Please enter or scan a VIN, or select Year, Make, and Model.");
            builder.show();
        }
    };
    View.OnClickListener clkGoToTPMS = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.tpmsmanager.com"));
            startActivity(i);
        }
    };
    public void tokenResult(boolean status, String msg){
        if(status){
            RetrievalModule.getInstance().getTPMS(this, byVIN);
        }else{
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){
                    dialog.dismiss();
                }
            });
            builder.setMessage(msg);
            builder.show();
        }
    }
    public void retrievalResult(String msg){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which){
                dialog.dismiss();
            }
        });
        builder.setMessage(msg);
        builder.show();
    }
    public void onActivityResult(int requestCode, int resultCode, Intent intent){
        switch(requestCode){
            default:
                IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
                if(scanResult != null){
                    if(scanResult.getContents() != null){
                        txtVIN.setText(scanResult.getContents().toString());
                    }
                }else{
                    Toast.makeText(this, "Your scan was unsuccessful.\nPlease try again.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
